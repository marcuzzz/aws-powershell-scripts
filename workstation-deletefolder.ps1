$thefolder = "C:\ProgramData\Thinkbox\Deadline10\workers"
$aduser = Get-ADUser 'admin' -Properties HomeDirectory
takeown /f $thefolder /a /r /d y /skipsl
cacls $thefolder /e /t /g administrators:f
Get-ChildItem -Path $thefolder -Recurse | Remove-Item -Force -Recurse 
Remove-Item $thefolder -Force

$thefolder = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp"
$aduser = Get-ADUser 'admin' -Properties HomeDirectory
takeown /f $thefolder /a /r /d y /skipsl
cacls $thefolder /e /t /g administrators:f
Get-ChildItem -Path $thefolder -Recurse | Remove-Item -Force -Recurse 