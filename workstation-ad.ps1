﻿$domain = $args[0]
$username = $args[1]
$password = $args[2] | ConvertTo-SecureString -asPlainText -Force
$username = "$domain\$username" 
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
Add-Computer -DomainName $domain -Credential $credential -Restart -Force