﻿$guid = New-Guid
$guid = "mm-" + $guid.toString().split('-')[0]

if ($args[0]){
    $newname= "mm-" + $args[0].Split('-')[-2] + "-" + $args[0].Split('-')[-1]
} else{
        
     $newname= $guid
}

$username = "monks\Admin"

if ($args[1]){
    $password = $args[1] | ConvertTo-SecureString -asPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential($username,$password)
}

#Write-Host $guid

$succes = 1

try {
    
    $share = Rename-Computer -NewName $newname -DomainCredential $credential -Restart -Force

    if($?) {
        $succes = 1
    } else {
        $succes = 0
    }


} catch {
     
    Write-Host "Failed to rename... " +  $newname
    $succes = 0
}


if($succes -eq 0) {

$succes = 1
   
try {
    
    $share = Remove-Computer -NewName $newname -UnjoinDomainCredential $credential
    $share02 = Rename-Computer -NewName $newname -DomainCredential $credential -Restart -Force

    if($?) {
        $succes = 1
    } else {
        $succes = 0
    }

} catch {
    
    Write-Host "Failed to remove + rename... " +  $newname
    $succes = 0
}

}



if($succes -eq 0) {

    $succes = 1
   
try {
    
    Rename-Computer -NewName $guid -DomainCredential $credential -Restart -Force

} catch {
    
    Write-Host "Failed to rename... " + $guid
    $succes = 0
}

}
 
