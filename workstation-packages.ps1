﻿[cmdletbinding()]
Param()


try {
    $ErrorActionPreference = "Stop"

    #
    # Install packages
    #
    Write-Verbose "Install Chocolatey"
    
    #$url = 'https://chocolatey.org/install.ps1'
    #Invoke-Expression ((new-object net.webclient).DownloadString($url))

    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

    #Write-Host "Install 7zip"
    #choco install --limit-output -y 7zip

    #Write-Host "Install awscli"
    #choco install --limit-output -y awscli
       
    #Write-Host "Install notepadplusplus"
    #choco install --limit-output -y notepadplusplus

    Write-Host "choco installs complete"

}
catch {
    Write-Host "catch: $_"
    $_ | Write-AWSQuickStartException
}
