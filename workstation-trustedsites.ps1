﻿  function Disable-InternetExplorerESC {
    $AdminKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
    $UserKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}"
    Set-ItemProperty -Path $AdminKey -Name "IsInstalled" -Value 0 -Force
    Set-ItemProperty -Path $UserKey -Name "IsInstalled" -Value 0 -Force
    Stop-Process -Name Explorer -Force
    Write-Host "IE Enhanced Security Configuration (ESC) has been disabled." -ForegroundColor Green
}
function Enable-InternetExplorerESC {
    $AdminKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
    $UserKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}"
    Set-ItemProperty -Path $AdminKey -Name "IsInstalled" -Value 1 -Force
    Set-ItemProperty -Path $UserKey -Name "IsInstalled" -Value 1 -Force
    Stop-Process -Name Explorer
    Write-Host "IE Enhanced Security Configuration (ESC) has been enabled." -ForegroundColor Green
}
function Disable-UserAccessControl {
    Set-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "ConsentPromptBehaviorAdmin" -Value 00000000 -Force
    Write-Host "User Access Control (UAC) has been disabled." -ForegroundColor Green    
}

function addSites{

    #1. Add site to trusted sites

    #Setting IExplorer settings
    Write-Verbose "Now configuring IE"

    #Navigate to the domains folder in the registry
    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item adobe.com/ -Force #website part without https
    set-location adobe.com/
    new-itemproperty . -Name * -Value 2 -Type DWORD -Force

    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item adobedtm.com/ -Force #website part without https
    set-location adobedtm.com/
    new-itemproperty . -Name * -Value 2 -Type DWORD -Force

    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item adobelogin.com/ -Force #website part without https
    set-location adobelogin.com/
    new-itemproperty . -Name * -Value 2 -Type DWORD -Force


    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item recaptcha.net/ -Force #website part without https
    set-location recaptcha.net/
    new-itemproperty . -Name * -Value 2 -Type DWORD -Force

    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item newrelic.com/ -Force #website part without https
    set-location newrelic.com/
    new-itemproperty . -Name * -Value 2 -Type DWORD -Force


    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item nr-data.net/ -Force #website part without https
    set-location nr-data.net/
    new-itemproperty . -Name * -Value 2 -Type DWORD -Force


    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item typekit.net/ -Force #website part without https
    set-location typekit.net/
    new-itemproperty . -Name * -Value 2 -Type DWORD -Force


    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item security_AfterFx.exe/ -Force #website part without https
    set-location security_AfterFx.exe/
    new-itemproperty . -Name about -Value 2 -Type DWORD -Force


    set-location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    set-location ZoneMap\EscDomains
    new-item "security_Adobe Desktop Service.exe/" -Force #website part without https
    set-location "security_Adobe Desktop Service.exe/"
    new-itemproperty . -Name about -Value 2 -Type DWORD -Force

    Write-Host "Site added Successfully"
    Start-Sleep -s 2

}

addSites
#Disable-UserAccessControl
#Disable-InternetExplorerESC 
 
